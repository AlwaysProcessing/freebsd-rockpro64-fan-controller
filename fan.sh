#!/bin/sh
while :
do
    cpu=$(sysctl -a | grep hw.temperature.CPU: | cut -c 21-22)
    gpu=$(sysctl -a | grep hw.temperature.GPU: | cut -c 21-22)
    let "val=(($cpu+$gpu)/2)"
    let "pwr=(40000+((10000-40000)/(60-38))*($val-38))"
    if (($val < 38)); then
        end=$(pwm -f pwmc1.0 -C | grep enabled | cut -c 9)
        if ! (($end == 0)); then
            $(pwm -D -f pwmc1.0 -p 0 -d 0)
        fi
    else
        $(pwm -E -f pwmc1.0 -p 50000 -d $pwr)
    fi
    sleep 10
done
