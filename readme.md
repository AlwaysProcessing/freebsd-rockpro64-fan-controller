FreeBSD RockPro64 Fan Controller
===

## Why

Why not? :thinking:

In all seriousness though. I prefer to have a fan adjust itself based on the temperature rather than manually adjusting it.

## How-To

1. Download using git clone
2. Make `fan.sh` executable with `chmod +x fan.sh`
3. Login as root
4. Edit cron using `crontab -e`
5. Add the following to Cron:

    >@reboot root nohup bash /home/freebsd/fan.sh </dev/null >&1 &

6. Exit, save and launch the script by restarting Cron or by entering `nohup bash /home/freebsd/fan.sh </dev/null >&1 &`

## Background

This script is based off of [ATS by tuxd3v](https://github.com/tuxd3v/ats)

The minimum temperature to activate the fan is 38°C and the script will check the temperature every 10 seconds.

## Side-Notes

None at this time... :shrug: